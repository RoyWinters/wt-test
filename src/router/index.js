import { createRouter, createWebHistory } from "vue-router";

const routes = [
  {
    path: "/",
    name: "Main",
    component: () => import("@/views/Posts.vue"),
  },
  {
    path: "/post/:id",
    name: "PostSingle",
    component: () => import("@/views/PostSingle.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;

const state = {
  authorModal: false,
  authorData: null,
};

const mutations = {
  openAuthorModal(state, { open, data }) {
    state.authorModal = open;
    if (data) state.authorData = data;
  },
};

export default {
  state,
  mutations,
};

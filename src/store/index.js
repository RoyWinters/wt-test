import { createStore } from "vuex";
import modalStore from "./modalStore";

export default createStore({
  modules: {
    modalStore,
  },
});
